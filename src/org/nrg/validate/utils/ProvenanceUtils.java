package org.nrg.validate.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ProvenanceUtils {

	public static String GetUser() {
		return  System.getProperty("user.name");
	}

	public static String GetOsName() {
		return  System.getProperty("os.name");
	}

	public static String GetHostName() {
		String rtn = "";
		try
		{
			java.net.InetAddress localMachine =
		java.net.InetAddress.getLocalHost();	
		  rtn =  localMachine.getHostName();
		}
		catch(java.net.UnknownHostException uhe)
		{
			//handle exception
		}
		return rtn;
	}
	
	public static String GetOsArch() {
		return  System.getProperty("os.arch");
	}
	public static String GetOsVersion() {
		return  System.getProperty("os.version");
	}

	public static String GetDateTimeStamp() {
		return  System.getProperty("os.version");
	}

	public static String GetDate() {
		Date date = new Date();
	    SimpleDateFormat sdf;
	    sdf = new SimpleDateFormat("yyyy-MM-dd");
	    return sdf.format(date);
	}
	
	public static String GetTime() {
		Date date = new Date();
	    SimpleDateFormat sdf;
	    sdf = new SimpleDateFormat("hh:mm:ss");
	    return sdf.format(date);
	}
	

	
	public static void main(String[] args) {
		System.out.println("user: " + GetUser());
		System.out.println("OS: " + GetOsName());
		System.out.println("arch: " + GetOsArch());
		System.out.println("version: " + GetOsVersion());
		System.out.println("hostname: " + GetHostName());
		System.out.println("Date is " + GetDate()+"T"+GetTime());
	}
	
}
